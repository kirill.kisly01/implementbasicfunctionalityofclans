### Запустить проект
Нужны следующие инструменты:

* Visual Studio 2019/2022
* .Net Core 5 or later
* Docker Desktop


### Установка ssl certificate
* [Документация на microsoft](https://docs.microsoft.com/en-us/aspnet/core/security/docker-https?view=aspnetcore-5.0)
* Windows
```csharp
dotnet dev-certs https -ep %USERPROFILE%\.aspnet\https\aspnetapp.pfx -p { password here }
dotnet dev-certs https --trust
```
* macOS or Linux
```csharp
dotnet dev-certs https -ep ${HOME}/.aspnet/https/aspnetapp.pfx -p { password here }
dotnet dev-certs https --trust
```
>{password here} заменить на SECRETPASSWORD

### Установка
1. Клонировать репозиторий
2. В корневом каталоге, включающем файлы **docker-compose.yml**, выполните следующие команды:
```csharp
docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d
```
3. Подождите, пока docker скомпонует все.
4. Вы можете **запустить микросервис** по следующей ссылке:
* **Clan API -> http://host.docker.internal:5000/swagger/index.html**
* **Clan API -> https://host.docker.internal:5001/swagger/index.html**

### Запуск через Visual Studio
1. Запустить в VS через docker-compose

