﻿using ClanManagement.API.Models;
using ClanManagement.Application.Features.Clans.Commands.AddMember;
using ClanManagement.Application.Features.Clans.Commands.CreateClan;
using ClanManagement.Application.Features.Clans.Commands.DeleteClan;
using ClanManagement.Application.Features.Clans.Commands.DeleteMember;
using ClanManagement.Application.Features.Clans.Commands.EditRank;
using ClanManagement.Application.Features.Clans.Commands.UpdateClan;
using ClanManagement.Application.Features.Clans.Queries.GetClan;
using ClanManagement.Application.Features.Clans.Queries.GetClansList;
using ClanManagement.Application.Model;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClanManagement.API.Controllers
{
    [Route("api/v1/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ClansController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ClansController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        /// <summary>
        /// Получение списка кланов и их участников
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetClans")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ClanViewModel>>> GetClans()
        {
            var query = new GetClansListQuery();
            var clans = await _mediator.Send(query);

            return Ok(clans);
        }

        /// <summary>
        /// Получение клана
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetClan")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ClanViewModel>>> GetClan(int id)
        {
            var query = new GetClanQuery(id);
            var clan = await _mediator.Send(query);

            return Ok(clan);
        }

        /// <summary>
        /// Создание клана
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost(Name = "CreateClan")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ClanViewModel>> CreateClan([FromBody] CreateClan request)
        {
            var command = new CreateClanCommand(request.Name, request.Description, request.MemberId);
            var entity = await _mediator.Send(command);

            return CreatedAtAction(nameof(GetClan), new { id = entity.Id }, entity);
        }

        /// <summary>
        /// Смена описание клана
        /// </summary>
        /// <param name="clanId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{clanId}", Name = "UpdateClan")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ClanUpdateViewModel>> UpdateClan(int clanId, [FromBody] UpdateClan request)
        {
            var command = new UpdateClanCommand(clanId, request.Description, request.MemberId);
            var clan = await _mediator.Send(command);

            return Ok(clan);
        }

        /// <summary>
        /// Удаление клана
        /// </summary>
        /// <param name="clanId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("{clanId}", Name = "DeleteClan")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteClan(int clanId, [FromBody] DeleteClan request)
        {
            var command = new DeleteClanCommand(clanId, request.MemberId);
            await _mediator.Send(command);

            return NoContent();
        }

        /// <summary>
        /// Добавление нового участника в клан
        /// </summary>
        /// <param name="clanId"></param>
        /// <param name="memberId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{clanId}/Members/{memberId}", Name = "AddMember")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddMember(int clanId, int memberId, [FromBody] AddMember request)
        {
            var command = new AddMemberCommand(clanId, memberId, request.AddMemberId);
            await _mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Повышение/понижение ролей участника клана
        /// </summary>
        /// <param name="clanId"></param>
        /// <param name="memberId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{clanId}/Members/{memberId}", Name = "EditRankMember")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MemberViewModel>> EditRankMember(int clanId, int memberId, [FromBody] EditRankMember request)
        {
            var command = new EditRankCommand(clanId, memberId, request.EditMemberId, request.Rank);
            var member = await _mediator.Send(command);

            return Ok(member);
        }

        /// <summary>
        /// Удаление участника клана
        /// </summary>
        /// <param name="clanId"></param>
        /// <param name="memberId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("{clanId}/Members/{memberId}", Name = "DeleteMember")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteMember(int clanId, int memberId, [FromBody] DeleteMember request)
        {
            var command = new DeleteMemberCommand(clanId, memberId, request.DeleteMemberId);
            await _mediator.Send(command);

            return NoContent();
        }
    }
}
