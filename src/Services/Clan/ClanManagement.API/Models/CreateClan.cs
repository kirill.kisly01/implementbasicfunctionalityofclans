﻿namespace ClanManagement.API.Models
{
    public class CreateClan
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MemberId { get; set; }
    }
}
