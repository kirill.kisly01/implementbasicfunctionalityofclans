﻿using ClanManagement.Domain.Enums;

namespace ClanManagement.API.Models
{
    public class EditRankMember
    {
        public int EditMemberId { get; set; }
        public ClanRank Rank { get; set; }
    }
}
