﻿namespace ClanManagement.API.Models
{
    public class UpdateClan
    {
        public int MemberId { get; set; }
        public string Description { get; set; }
    }
}
