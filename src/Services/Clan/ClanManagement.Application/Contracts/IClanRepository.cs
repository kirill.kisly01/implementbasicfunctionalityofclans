﻿using ClanManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ClanManagement.Application.Contracts
{
    public interface IClanRepository : IAsyncRepository<Clan>
    {
        Task<IEnumerable<Clan>> GetClans();
        Task<Clan> GetClanById(int id);
        Task<Clan> GetClanById(int id, Expression<Func<Clan, object>> include);
    }
}
