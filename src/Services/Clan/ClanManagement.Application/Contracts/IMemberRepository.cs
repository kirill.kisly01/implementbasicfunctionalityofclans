﻿using ClanManagement.Domain.Entities;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ClanManagement.Application.Contracts
{
    public interface IMemberRepository : IAsyncRepository<Member>
    {
        Task<Member> GetMemberById(int id);
        Task<Member> GetMemberById(int id, Expression<Func<Member, object>> include);
    }
}
