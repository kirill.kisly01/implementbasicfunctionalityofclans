﻿using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.AddMember
{
    public record AddMemberCommand(int ClanId, int MemberId, int AddMemberId) : IRequest;
}
