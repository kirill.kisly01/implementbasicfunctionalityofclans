﻿using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Features.Clans.Extensions;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.AddMember
{
    public class AddMemberCommandHandler : IRequestHandler<AddMemberCommand>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMemberRepository _memberRepository;

        public AddMemberCommandHandler(IClanRepository clanRepository, IMemberRepository memberRepository)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _memberRepository = memberRepository ?? throw new ArgumentNullException(nameof(memberRepository));
        }

        public async Task<Unit> Handle(AddMemberCommand request, CancellationToken cancellationToken)
        {
            var addMember = await _memberRepository.GetMemberById(request.AddMemberId, x => x.Clans);
            if (addMember == null)
            {
                throw new NotFoundException(nameof(Member), request.AddMemberId);
            }

            if (addMember.Clans.Count == 0 || addMember.Clans.Any(x => x.Id != request.ClanId))
            {
                var clan = await _clanRepository.GetClanById(request.ClanId, x => x.Members);
                if (clan == null)
                {
                    throw new NotFoundException(nameof(Clan), request.ClanId);
                }

                if (!clan.CheckAccess(request.MemberId, ClanRank.Clanleader, ClanRank.Lieutenant, ClanRank.Soldier))
                {
                    throw new BadRequestException("You do not have enough rights.");
                }

                addMember.RankId = (int)ClanRank.Soldier;
                addMember.Clans?.Clear();
                clan.Members.Add(addMember);

                await _clanRepository.UpdateAsync(clan);
            }

            return Unit.Value;
        }
    }
}
