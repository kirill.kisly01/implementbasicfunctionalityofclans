﻿using ClanManagement.Application.Model;
using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.CreateClan
{
    public record CreateClanCommand(string Name, string Description, int MemberId)
        : IRequest<ClanViewModel>;
}
