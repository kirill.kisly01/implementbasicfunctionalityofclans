﻿using AutoMapper;
using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Model;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.CreateClan
{
    public class CreateClanCommandHandler : IRequestHandler<CreateClanCommand, ClanViewModel>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMemberRepository _memberRepository;
        private readonly IMapper _mapper;

        public CreateClanCommandHandler(
            IClanRepository clanRepository,
            IMemberRepository memberRepository,
            IMapper mapper)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _memberRepository = memberRepository ?? throw new ArgumentNullException(nameof(memberRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ClanViewModel> Handle(CreateClanCommand request, CancellationToken cancellationToken)
        {
            var clanCreator = await _memberRepository.GetMemberById(request.MemberId, x => x.Clans);
            if (clanCreator == null)
            {
                throw new NotFoundException(nameof(Member), request.MemberId);
            }
            clanCreator.RankId = (int)ClanRank.Clanleader;
            clanCreator.Clans?.Clear();

            var clanEntity = _mapper.Map<Clan>(request);
            clanEntity.Members = new List<Member> { clanCreator };

            var newClan = await _clanRepository.AddAsync(clanEntity);

            return _mapper.Map<ClanViewModel>(newClan);
        }
    }
}
