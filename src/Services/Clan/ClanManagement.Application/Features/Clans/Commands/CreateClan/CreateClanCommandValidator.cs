﻿using FluentValidation;

namespace ClanManagement.Application.Features.Clans.Commands.CreateClan
{
    public class CreateClanCommandValidator : AbstractValidator<CreateClanCommand>
    {
        public CreateClanCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("Name is required.")
                .NotNull()
                .MaximumLength(12).WithMessage("Name must not exceed 12 characters.")
                .Matches(@"^[\w0-9]*$").WithMessage("Name without special characters and spaces");

            RuleFor(p => p.Description)
                .MaximumLength(30).WithMessage("Description must not exceed 30 characters.");
        }
    }
}
