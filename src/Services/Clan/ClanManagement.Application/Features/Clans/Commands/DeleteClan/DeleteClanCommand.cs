﻿using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.DeleteClan
{
    public record DeleteClanCommand(int Id, int MemberId) : IRequest;
}
