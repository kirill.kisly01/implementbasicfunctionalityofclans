﻿using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Features.Clans.Extensions;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.DeleteClan
{
    public class DeleteClanCommandHandler : IRequestHandler<DeleteClanCommand>
    {
        private readonly IClanRepository _clanRepository;

        public DeleteClanCommandHandler(IClanRepository clanRepository)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
        }

        public async Task<Unit> Handle(DeleteClanCommand request, CancellationToken cancellationToken)
        {
            var clanToDelete = await _clanRepository.GetClanById(request.Id, x => x.Members);
            if (clanToDelete == null)
            {
                throw new NotFoundException(nameof(Clan), request.Id);
            }

            if (clanToDelete.CheckAccess(request.MemberId, ClanRank.Clanleader))
            {
                await _clanRepository.DeleteAsync(clanToDelete);

                return Unit.Value;
            }

            throw new BadRequestException("You cannot delete a clan.");
        }
    }
}
