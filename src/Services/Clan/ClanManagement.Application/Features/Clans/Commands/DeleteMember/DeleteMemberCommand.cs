﻿using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.DeleteMember
{
    public record DeleteMemberCommand(int ClanId, int MemberId, int DeleteMemberId) : IRequest;
}
