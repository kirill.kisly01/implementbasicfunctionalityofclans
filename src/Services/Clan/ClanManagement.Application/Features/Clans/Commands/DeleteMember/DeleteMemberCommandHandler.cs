﻿using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Features.Clans.Extensions;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.DeleteMember
{
    public class DeleteMemberCommandHandler : IRequestHandler<DeleteMemberCommand>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMemberRepository _memberRepository;

        public DeleteMemberCommandHandler(IClanRepository clanRepository, IMemberRepository memberRepository)
        {
            _clanRepository = clanRepository;
            _memberRepository = memberRepository;
        }

        public async Task<Unit> Handle(DeleteMemberCommand request, CancellationToken cancellationToken)
        {
            var deleteMember = await _memberRepository.GetMemberById(request.DeleteMemberId, x => x.Clans);
            if (deleteMember == null)
            {
                throw new NotFoundException(nameof(Member), request.DeleteMemberId);
            }

            if (deleteMember.Clans.Any(x => x.Id == request.ClanId))
            {
                if (deleteMember.RankId != (int)ClanRank.Soldier)
                {
                    throw new BadRequestException("Only clan members with the role of soldiers can be removed.");
                }

                var clan = await _clanRepository.GetClanById(request.ClanId, x => x.Members);
                if (clan == null)
                {
                    throw new NotFoundException(nameof(Clan), request.ClanId);
                }

                if (!clan.CheckAccess(request.MemberId, ClanRank.Clanleader))
                {
                    throw new BadRequestException("You do not have enough rights.");
                }

                clan.Members.Remove(deleteMember);

                await _clanRepository.UpdateAsync(clan);
            }

            return Unit.Value;
        }
    }
}
