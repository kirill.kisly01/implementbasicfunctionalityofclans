﻿using ClanManagement.Application.Model;
using ClanManagement.Domain.Enums;
using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.EditRank
{
    public record EditRankCommand(int ClanId, int MemberId, int EditMemberId, ClanRank Rank) 
        : IRequest<MemberViewModel>;
}
