﻿using AutoMapper;
using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Model;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.EditRank
{
    public class EditRankCommandHandler : IRequestHandler<EditRankCommand, MemberViewModel>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMemberRepository _memberRepository;
        private readonly IMapper _mapper;

        public EditRankCommandHandler(
            IClanRepository clanRepository,
            IMemberRepository memberRepository,
            IMapper mapper)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _memberRepository = memberRepository ?? throw new ArgumentNullException(nameof(memberRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<MemberViewModel> Handle(EditRankCommand request, CancellationToken cancellationToken)
        {
            var editMember = await _memberRepository.GetMemberById(request.EditMemberId, x => x.Clans);
            if (editMember == null)
            {
                throw new NotFoundException(nameof(Member), request.EditMemberId);
            }

            if (editMember.Clans.Any(x => x.Id == request.ClanId))
            {
                var editorMember = await _memberRepository.GetByIdAsync(request.MemberId);
                if (editorMember == null)
                {
                    throw new NotFoundException(nameof(Member), editorMember.Id);
                }

                if (editMember.Id == editorMember.Id)
                {
                    throw new BadRequestException("You cannot change his rank.");
                }

                return editorMember.RankId switch
                {
                    (int)ClanRank.Clanleader => await ClanleaderEditRank(editMember, request.Rank),
                    (int)ClanRank.Lieutenant => await LieutenantUpRank(editMember, request.Rank),
                    _ => throw new BadRequestException("You cannot change his rank."),
                };
            }

            throw new BadRequestException("You cannot change his rank.");
        }

        private async Task<MemberViewModel> ClanleaderEditRank(Member editMember, ClanRank rank)
        {
            EditRank(editMember, rank);
            await _memberRepository.UpdateAsync(editMember);

            return _mapper.Map<MemberViewModel>(editMember);
        }

        private async Task<MemberViewModel> LieutenantUpRank(Member editMember, ClanRank rank)
        {
            if (rank == ClanRank.Lieutenant && editMember.RankId != (int)ClanRank.Clanleader)
            {
                EditRank(editMember, rank);
                await _memberRepository.UpdateAsync(editMember);

                return _mapper.Map<MemberViewModel>(editMember);
            }

            throw new BadRequestException("You cannot change his rank.");
        }

        private readonly Action<Member, ClanRank> EditRank = (editMember, rank) =>
            editMember.RankId = (int)rank;
    }
}
