﻿using ClanManagement.Application.Model;
using MediatR;

namespace ClanManagement.Application.Features.Clans.Commands.UpdateClan
{
    public record UpdateClanCommand(int Id, string Description, int MemberId) 
        : IRequest<ClanUpdateViewModel>;
}
