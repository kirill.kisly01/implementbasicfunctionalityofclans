﻿using AutoMapper;
using ClanManagement.Application.Contracts;
using ClanManagement.Application.Exceptions;
using ClanManagement.Application.Features.Clans.Extensions;
using ClanManagement.Application.Model;
using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Commands.UpdateClan
{
    public class UpdateClanCommandHandler : IRequestHandler<UpdateClanCommand, ClanUpdateViewModel>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMapper _mapper;

        public UpdateClanCommandHandler(IClanRepository clanRepository, IMapper mapper)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ClanUpdateViewModel> Handle(UpdateClanCommand request, CancellationToken cancellationToken)
        {
            var clanToUpdate = await _clanRepository.GetClanById(request.Id, x => x.Members);
            if (clanToUpdate == null)
            {
                throw new NotFoundException(nameof(Clan), request.Id);
            }

            if (clanToUpdate.CheckAccess(request.MemberId, ClanRank.Clanleader, ClanRank.Lieutenant))
            {
                _mapper.Map(request, clanToUpdate, typeof(UpdateClanCommand), typeof(Clan));
                await _clanRepository.UpdateAsync(clanToUpdate);

                return _mapper.Map<ClanUpdateViewModel>(clanToUpdate);
            }

            throw new BadRequestException("You cannot update the description in the clan.");
        }
    }
}
