﻿using FluentValidation;

namespace ClanManagement.Application.Features.Clans.Commands.UpdateClan
{
    public class UpdateClanCommandValidator : AbstractValidator<UpdateClanCommand>
    {
        public UpdateClanCommandValidator()
        {
            RuleFor(p => p.Description)
                .MaximumLength(30).WithMessage("Description must not exceed 30 characters.");
        }
    }
}
