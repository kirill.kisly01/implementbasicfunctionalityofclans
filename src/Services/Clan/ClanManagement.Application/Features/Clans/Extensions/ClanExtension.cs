﻿using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using System;
using System.Linq;

namespace ClanManagement.Application.Features.Clans.Extensions
{
    public static class ClanExtension
    {
        public static bool CheckAccess(this Clan clan, int memberId, params ClanRank[] ranks)
        {
            if (clan.Members.Any(x => x.Id == memberId && ranks.Contains((ClanRank)x.RankId)))
            {
                return true;
            }

            return false;
        }
    }
}
