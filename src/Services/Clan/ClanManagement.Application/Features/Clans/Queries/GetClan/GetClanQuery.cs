﻿using ClanManagement.Application.Model;
using MediatR;

namespace ClanManagement.Application.Features.Clans.Queries.GetClan
{
    public record GetClanQuery(int Id) : IRequest<ClanViewModel>;
}
