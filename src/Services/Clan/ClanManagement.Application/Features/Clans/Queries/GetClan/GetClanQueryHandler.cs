﻿using AutoMapper;
using ClanManagement.Application.Contracts;
using ClanManagement.Application.Model;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Queries.GetClan
{
    public class GetClanQueryHandler : IRequestHandler<GetClanQuery, ClanViewModel>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMapper _mapper;

        public GetClanQueryHandler(IClanRepository clanRepository, IMapper mapper)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ClanViewModel> Handle(GetClanQuery request, CancellationToken cancellationToken)
        {
            var clan = await _clanRepository.GetClanById(request.Id, x => x.Members);

            return _mapper.Map<ClanViewModel>(clan);
        }
    }
}
