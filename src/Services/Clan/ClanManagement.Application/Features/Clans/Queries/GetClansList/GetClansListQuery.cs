﻿using ClanManagement.Application.Model;
using MediatR;
using System.Collections.Generic;

namespace ClanManagement.Application.Features.Clans.Queries.GetClansList
{
    public record GetClansListQuery() : IRequest<List<ClanViewModel>>;
}
