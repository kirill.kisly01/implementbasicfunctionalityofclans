﻿using AutoMapper;
using ClanManagement.Application.Contracts;
using ClanManagement.Application.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClanManagement.Application.Features.Clans.Queries.GetClansList
{
    public class GetClansListQueryHandler : IRequestHandler<GetClansListQuery, List<ClanViewModel>>
    {
        private readonly IClanRepository _clanRepository;
        private readonly IMapper _mapper;

        public GetClansListQueryHandler(IClanRepository clanRepository, IMapper mapper)
        {
            _clanRepository = clanRepository ?? throw new ArgumentNullException(nameof(clanRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ClanViewModel>> Handle(GetClansListQuery request, CancellationToken cancellationToken)
        {
            var clansList = await _clanRepository.GetClans();

            return _mapper.Map<List<ClanViewModel>>(clansList);
        }
    }
}
