﻿using AutoMapper;
using ClanManagement.Application.Features.Clans.Commands.CreateClan;
using ClanManagement.Application.Features.Clans.Commands.UpdateClan;
using ClanManagement.Application.Model;
using ClanManagement.Domain.Entities;

namespace ClanManagement.Application.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Clan, ClanViewModel>().ReverseMap();
            CreateMap<Clan, ClanUpdateViewModel>().ReverseMap();
            CreateMap<Member, MemberViewModel>().ReverseMap();

            CreateMap<Clan, CreateClanCommand>().ReverseMap();
            CreateMap<Clan, UpdateClanCommand>().ReverseMap();
        }
    }
}
