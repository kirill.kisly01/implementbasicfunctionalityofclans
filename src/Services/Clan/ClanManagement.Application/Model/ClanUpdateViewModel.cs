﻿namespace ClanManagement.Application.Model
{
    public class ClanUpdateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
