﻿namespace ClanManagement.Application.Model
{
    public class MemberViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RankId { get; set; }
    }
}
