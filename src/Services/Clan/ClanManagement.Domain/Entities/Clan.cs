﻿using ClanManagement.Domain.Common;
using System.Collections.Generic;

namespace ClanManagement.Domain.Entities
{
    public class Clan : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Member> Members { get; set; }
        public ICollection<Membership> Memberships { get; set; }
    }
}
