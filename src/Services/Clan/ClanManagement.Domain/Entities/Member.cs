﻿using ClanManagement.Domain.Common;
using System.Collections.Generic;

namespace ClanManagement.Domain.Entities
{
    public class Member : EntityBase
    {
        public string Name { get; set; }
        public int RankId { get; set; }

        public ICollection<Clan> Clans { get; set; }
        public ICollection<Membership> Memberships { get; set; }
    }
}
