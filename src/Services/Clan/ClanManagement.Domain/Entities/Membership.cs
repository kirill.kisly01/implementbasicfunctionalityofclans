﻿using ClanManagement.Domain.Common;

namespace ClanManagement.Domain.Entities
{
    public class Membership : EntityBase
    {
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        public int MemberId { get; set; }
        public Member Member { get; set; }
    }
}
