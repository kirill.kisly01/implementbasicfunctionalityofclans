﻿using ClanManagement.Domain.Common;
using ClanManagement.Domain.Enums;

namespace ClanManagement.Domain.Entities
{
    public class Rank : EntityBase
    {
        public ClanRank Name { get; set; }
        public string Description { get; set; }
    }
}
