﻿namespace ClanManagement.Domain.Enums
{
    public enum ClanRank
    {
        Clanleader = 1,
        Lieutenant,
        Soldier
    }
}
