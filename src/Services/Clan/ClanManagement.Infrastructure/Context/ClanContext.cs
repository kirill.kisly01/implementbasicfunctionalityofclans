﻿using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;

namespace ClanManagement.Infrastructure.Context
{
    public class ClanContext : DbContext
    {
        public ClanContext(DbContextOptions<ClanContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Rank>()
                .Property(x => x.Name)
                .HasConversion(
                    v => v.ToString(),
                    v => (ClanRank)Enum.Parse(typeof(ClanRank), v));

            modelBuilder
                .Entity<Clan>()
                .HasMany(m => m.Members)
                .WithMany(c => c.Clans)
                .UsingEntity<Membership>(
                    j => j
                        .HasOne(x => x.Member)
                        .WithMany(x => x.Memberships)
                        .HasForeignKey(x => x.MemberId),
                    j => j
                        .HasOne(x => x.Clan)
                        .WithMany(x => x.Memberships)
                        .HasForeignKey(x => x.ClanId),
                    j =>
                    {
                        j.HasKey(x => new { x.ClanId, x.MemberId });
                        j.ToTable("Memberships");
                    });
        }

        public DbSet<Clan> Clans { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Membership> Memberships { get; set; }
    }
}
