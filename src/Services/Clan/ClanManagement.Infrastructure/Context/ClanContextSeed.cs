﻿using ClanManagement.Domain.Entities;
using ClanManagement.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClanManagement.Infrastructure.Context
{
    public static class ClanContextSeed
    {
        public static async Task SeedAsync(IServiceProvider serviceProvider)
        {
            using var context = new ClanContext(serviceProvider.GetRequiredService<DbContextOptions<ClanContext>>());
            context.Database.Migrate();

            if (!context.Ranks.Any())
            {
                context.Ranks.AddRange(GetInitRanks());
                await context.SaveChangesAsync();
            }

            if (!context.Clans.Any())
            {
                context.Clans.AddRange(GetInitClans());
                await context.SaveChangesAsync();
            }

            if (!context.Members.Any())
            {
                context.Members.AddRange(GetInitMembers());
                await context.SaveChangesAsync();
            }

            if (!context.Memberships.Any())
            {
                context.Memberships.AddRange(GetInitMemberships());
                await context.SaveChangesAsync();
            }
        }

        #region initData

        private static IEnumerable<Clan> GetInitClans()
        {
            return new List<Clan>
            {
                new Clan() { Id = 1, Name = "Immortal", Description = "Some description 1..." },
                new Clan() { Id = 2, Name = "ChaosLegion", Description = "Some description 2..." },
                new Clan() { Id = 3, Name = "Goldarmy", Description = "Some description 3..." }
            };
        }

        private static IEnumerable<Member> GetInitMembers()
        {
            return new List<Member>
            {
                new Member() { Id = 1, Name = "Ivan", RankId = 1 },
                new Member() { Id = 2, Name = "Petr", RankId = 2 },
                new Member() { Id = 3, Name = "Username", RankId = 2 },
                new Member() { Id = 4, Name = "Member43", RankId = 3 },
                new Member() { Id = 5, Name = "beta", RankId = 1 },
                new Member() { Id = 6, Name = "lemon", RankId = 3 },
                new Member() { Id = 7, Name = "alfa", RankId = 1 },
                new Member() { Id = 8, Name = "Endy", RankId = 3 },
                new Member() { Id = 9, Name = "Drake" },
                new Member() { Id = 10, Name = "Member10" },
                new Member() { Id = 11, Name = "Member11" },
                new Member() { Id = 12, Name = "Member12" },
                new Member() { Id = 13, Name = "Member13" },
                new Member() { Id = 14, Name = "Member14" },
                new Member() { Id = 15, Name = "Member15" }
            };
        }

        private static IEnumerable<Rank> GetInitRanks()
        {
            return new List<Rank>
            {
                new Rank()
                {
                    Id = 1,
                    Name = ClanRank.Clanleader,
                    Description = "Может редактировать описание клана, удалять клан, удалять других участников, повышать или понижать в звании других участников клана."
                },
                new Rank()
                {
                    Id = 2,
                    Name = ClanRank.Lieutenant,
                    Description = "Может редактировать описание клана, повышать до заместителя."
                },
                new Rank()
                {
                    Id = 3,
                    Name = ClanRank.Soldier,
                    Description = "Солдат не имеет никаких привилегий."
                }
            };
        }

        private static IEnumerable<Membership> GetInitMemberships()
        {
            return new List<Membership>
            {
                new Membership() { Id = 1, ClanId = 1, MemberId = 1 },
                new Membership() { Id = 2, ClanId = 1, MemberId = 2 },
                new Membership() { Id = 3, ClanId = 1, MemberId = 3 },
                new Membership() { Id = 4, ClanId = 1, MemberId = 4 },
                new Membership() { Id = 5, ClanId = 2, MemberId = 5 },
                new Membership() { Id = 6, ClanId = 2, MemberId = 6 },
                new Membership() { Id = 6, ClanId = 3, MemberId = 7 },
                new Membership() { Id = 7, ClanId = 3, MemberId = 8 }
            };
        }

        #endregion
    }
}
