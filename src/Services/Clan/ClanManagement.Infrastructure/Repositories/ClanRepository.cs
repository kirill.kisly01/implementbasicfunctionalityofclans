﻿using ClanManagement.Application.Contracts;
using ClanManagement.Domain.Entities;
using ClanManagement.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ClanManagement.Infrastructure.Repositories
{
    public class ClanRepository : RepositoryBase<Clan>, IClanRepository
    {
        public ClanRepository(ClanContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Clan>> GetClans()
        {
            var clanList = await _dbContext.Clans
                .Include(x => x.Members)
                .ToListAsync();

            return clanList;
        }

        public async Task<Clan> GetClanById(int id)
        {
            var clan = await _dbContext.Clans.FindAsync(id);

            return clan;
        }

        public async Task<Clan> GetClanById(int id, Expression<Func<Clan, object>> include)
        {
            var clans = await _dbContext.Clans.Include(include).ToListAsync();

            return clans.FirstOrDefault(x => x.Id == id);
        }
    }
}
