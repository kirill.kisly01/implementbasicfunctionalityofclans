﻿using ClanManagement.Application.Contracts;
using ClanManagement.Domain.Entities;
using ClanManagement.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ClanManagement.Infrastructure.Repositories
{
    public class MemberRepository : RepositoryBase<Member>, IMemberRepository
    {
        public MemberRepository(ClanContext dbContext) : base(dbContext)
        {
        }

        public async Task<Member> GetMemberById(int id)
        {
            var member = await _dbContext.Members.FindAsync(id);

            return member;
        }

        public async Task<Member> GetMemberById(int id, Expression<Func<Member, object>> include)
        {
            var members = await _dbContext.Members.Include(include).ToListAsync();

            return members.FirstOrDefault(x => x.Id == id);
        }
    }
}
